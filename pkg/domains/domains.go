package domains

type MyJobs struct {
	Jobs []struct {
		Img  string `json:"img"`
		Item []struct {
			Country  string `json:"country"`
			Details  string `json:"details"`
			Duration string `json:"duration"`
			Location string `json:"location"`
			Position string `json:"position"`
			Rank     int    `json:"rank"`
			Title    string `json:"title"`
		} `json:"item"`
		Name string `json:"name"`
		Path string `json:"path"`
	} `json:"jobs"`
	Maintitle string `json:"maintitle"`
	Subtitle  string `json:"subtitle"`
}