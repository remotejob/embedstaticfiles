package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/remotejob/embedstaticfiles/pkg/box"
	"gitlab.com/remotejob/embedstaticfiles/pkg/domains"
	_ "github.com/mattn/go-sqlite3"
)

func Post(w http.ResponseWriter, r *http.Request) {

	log.Println("Start Embed")

	var res domains.MyJobs
	// json.Unmarshal([]byte(index), &res)
	json.Unmarshal(box.Get("/jobs.json"), &res)
	

	js, err := json.Marshal(res)
	if err != nil {
		log.Panicln(err)
	}

	database, _ := sql.Open("sqlite3", string(box.Get("/jonssql.db")))
	if err != nil {
		log.Panicln(err)
	}

	rows, _ := database.Query("SELECT id, firstname, lastname FROM people")
	var id int
    var firstname string
    var lastname string
    for rows.Next() {
        rows.Scan(&id, &firstname, &lastname)
        fmt.Println(strconv.Itoa(id) + ": " + firstname + " " + lastname)
    }

	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(js))
}
